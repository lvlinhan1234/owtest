import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  scrollVHidden: false,
};

const globalSlicer = createSlice({
  name: "global",
  initialState: initialState,
  reducers: {
    hideScrollV: (state) => {
      state.scrollVHidden = true;
    },
    showScrollV: (state) => {
      state.scrollVHidden = false;
    },
  },
});

export const { hideScrollV, showScrollV } = globalSlicer.actions;
export default globalSlicer.reducer;
