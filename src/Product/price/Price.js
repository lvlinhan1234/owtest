import classes from "./Price.module.css";

const Price = () => {
  return (
    <div className={classes.container}>
      <div className={classes.price}>From $199.00</div>
      <div className={classes.tag}>
        <span>
          or Zip it from <b>$10/week</b> with
        </span>
        <img
          className={classes["pay-logo"]}
          src="https://static.zipmoney.com.au/assets/default/product-widget/img/zip-button-wht.svg"
          alt="pay-logo"
        ></img>
      </div>
    </div>
  );
};

export default Price;
