import React, { useEffect, useRef, useState } from "react";
import {
  BrowserView,
  MobileView,
  isBrowser,
  isMobile,
} from "react-device-detect";
import {
  AiOutlineZoomIn,
  AiOutlineZoomOut,
  AiOutlineClose,
} from "react-icons/ai";
import classes from "./Product-Zoom.module.css";
import { Carousel } from "react-responsive-carousel";
import { d2 } from "../tool/tool";
import { useDispatch, useSelector } from "react-redux";
import { hideScrollV, showScrollV } from "../state";

const ProductZoom = ({ productImages, setShowZoom, index = 0 }) => {
  const zoomBase = 1;
  const zoomStep = 0.1;
  const zoomLimit = 1.5;
  const dispatch = useDispatch();
  const [zoom, setZoom] = useState(zoomBase);
  const [zoom300msLater, setZoom300msLater] = useState(zoomBase);
  const [currentIndex, setCurrentIndex] = useState(index);
  const corouselRef = useRef();
  const [dragging, setDragging] = useState(false);
  const [touchRecord, setTouchRecord] = useState({});

  const [position, setPosition] = useState({
    left: 0,
    top: 0,
  });
  const [translate, setTranslate] = useState({
    xTranslate: 0,
    yTranslate: 0,
  });
  const [translateLimit, setTranslateLimit] = useState({
    xLimit: 0,
    yLimit: 0,
  });

  const currentContainerRef = useRef(null);
  const currentImgRef = useRef(null);

  const zoomUP = () => {
    if (zoom >= zoomLimit) return;
    setZoom((zoom) => d2(zoom + zoomStep));
  };
  const zoomMinMaxSwitch = () => {
    if (zoom < zoomLimit) setZoom(zoomLimit);
    else setZoom(zoomBase);
  };
  const zoomDown = () => {
    if (zoom === zoomBase) return;
    setZoom((zoom) => d2(zoom - zoomStep));
  };

  useEffect(() => {
    dispatch(hideScrollV());
    if (currentIndex != 0) corouselRef.current.moveTo(currentIndex);
    return () => {
      dispatch(showScrollV());
    };
  }, []);

  useEffect(() => {
    const container = document.getElementById("aaaaa");
    const containerWidth = container.offsetWidth;
    const containerHeight = container.offsetHeight;

    let xDifference = d2((containerWidth * (zoom - zoomBase)) / 2);
    const xScope = xDifference > 0 ? xDifference : 0;

    let yDifference = d2((containerHeight * (zoom - zoomBase)) / 2);
    const yScope = yDifference > 0 ? yDifference : 0;

    const left = -xDifference;
    const top = -yDifference;

    setTranslateLimit({
      xLimit: xScope,
      yLimit: yScope,
    });

    setPosition({
      left: left,
      top: top,
    });

    setTranslate({
      xTranslate:
        Math.abs(translate.xTranslate) <= xScope
          ? translate.xTranslate
          : translate.xTranslate > 0
          ? xScope
          : -xScope,
      yTranslate:
        Math.abs(translate.yTranslate) <= yScope
          ? translate.yTranslate
          : translate.yTranslate > 0
          ? yScope
          : -yScope,
    });
    const timer = setTimeout(() => {
      setZoom300msLater(zoom);
    }, 300);
    return () => clearTimeout(timer);
  }, [zoom]);

  return (
    <div className={classes.microscope}>
      <Carousel
        showArrows={false}
        autoPlay={false}
        infiniteLoop={true}
        // emulateTouch={true}
        showThumbs={false}
        dynamicHeight={false}
        showStatus={false}
        emulateTouch={zoom === zoomBase ? true : false}
        swipeable={zoom300msLater === zoomBase ? true : false}
        ref={corouselRef}
        onChange={(index) => {
          setCurrentIndex(index);
          setZoom(1);
        }}
      >
        {productImages.map((productImage, index) => (
          <div
            className={classes["img-container"]}
            key={productImage}
            ref={index === currentIndex ? currentContainerRef : undefined}
            id={index === currentIndex ? "aaaaa" : undefined}
          >
            <img
              src={productImage}
              alt="glass-img"
              ref={currentIndex === index ? currentImgRef : undefined}
              draggable={false}
              style={{
                width: window.innerWidth * zoom + "px",
                height: window.innerHeight * zoom + "px",
                left: position.left + "px",
                top: position.top + "px",
                transform: `translate(${translate.xTranslate}px,${translate.yTranslate}px)`,
                transition: dragging ? "none" : "",
              }}
              onMouseDown={(e) => {
                // if (zoom === 1) return;
                setDragging(true);
              }}
              onMouseUp={(e) => {
                setDragging(false);
              }}
              onMouseOut={(e) => {
                setDragging(false);
              }}
              onMouseMove={(e) => {
                if (dragging && !isMobile) {
                  const xDifference = e.movementX;
                  const yDifference = e.movementY;
                  setTranslate((translate) => {
                    let xNewTranslate = translate.xTranslate + xDifference;
                    if (Math.abs(xNewTranslate) > translateLimit.xLimit)
                      xNewTranslate = translate.xTranslate;

                    let yNewTranslate = translate.yTranslate + yDifference;
                    if (Math.abs(yNewTranslate) > translateLimit.yLimit)
                      yNewTranslate = translate.yTranslate;
                    return {
                      xTranslate: xNewTranslate,
                      yTranslate: yNewTranslate,
                    };
                  });
                }
              }}
              onDoubleClick={(e) => {
                zoomMinMaxSwitch();
              }}
              onTouchStart={(e) => {
                if (zoom === zoomBase) return;
                setDragging(true);
                setTouchRecord({
                  xRecord: e.targetTouches[0].clientX,
                  yRecord: e.targetTouches[0].clientY,
                });
              }}
              onTouchEnd={(e) => {
                setDragging(false);
              }}
              onTouchMove={(e) => {
                if (dragging) {
                  const xDifference =
                    e.targetTouches[0].clientX - touchRecord.xRecord;
                  const yDifference =
                    e.targetTouches[0].clientY - touchRecord.yRecord;
                  setTranslate((translate) => {
                    let xNewTranslate = translate.xTranslate + xDifference;
                    if (Math.abs(xNewTranslate) > translateLimit.xLimit)
                      xNewTranslate = translate.xTranslate;

                    let yNewTranslate = translate.yTranslate + yDifference;
                    if (Math.abs(yNewTranslate) > translateLimit.yLimit)
                      yNewTranslate = translate.yTranslate;
                    return {
                      xTranslate: xNewTranslate,
                      yTranslate: yNewTranslate,
                    };
                  });

                  setTouchRecord({
                    xRecord: e.targetTouches[0].clientX,
                    yRecord: e.targetTouches[0].clientY,
                  });
                }
              }}
            />
            {/* <p className="legend">Legend 1</p> */}
          </div>
        ))}
      </Carousel>
      <AiOutlineClose
        className={classes.closeZoomBtn}
        onClick={() => setShowZoom(false)}
        size={50}
      />

      <AiOutlineZoomIn
        className={`${classes.zoomUp} ${
          zoom === zoomLimit ? classes.pale : ""
        }`}
        onClick={zoomUP}
        size={50}
      />
      <AiOutlineZoomOut
        className={`${classes.zoomDown} ${
          zoom === zoomBase ? classes.pale : ""
        }`}
        onClick={zoomDown}
        size={50}
      />
    </div>
  );
};

export default ProductZoom;
