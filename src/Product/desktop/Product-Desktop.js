import ProductGallery from "../Product-Gallery";
import classes from "./Product-Desktop.module.css";
import ColerCollection from "../color/Color-Collection";
import Price from "../price/Price";
import Action from "../action/Action";
import DescriptionDesktop from "../description/Description-Desktop";

const ProductDesktop = (props) => {
  return (
    <div className={classes.container}>
      <div className={classes.main}>
        <div className={classes["left-panel"]}>
          <ProductGallery productImages={props.productImages} />
        </div>
        <div className={classes["right-panel"]}>
          <div className={classes.heading}>
            <p className={classes.discount}>2 pairs from $199</p>
            <p className={classes.tag}>Discount auto-applied at checkout.</p>
            <h1 className={classes.title}>MARCO</h1>
          </div>
          <div className={classes["color-collection-container"]}>
            <ColerCollection
              isDesktopOrLaptop={true}
              productColors={props.productColors}
              colorSelectedIndex={props.colorSelectedIndex}
              onColorSelect={props.onColorSelect}
            />
          </div>
          <div className={classes["price-container"]}>
            <Price />
          </div>
          <Action />
        </div>
      </div>
      <DescriptionDesktop />
    </div>
  );
};

export default ProductDesktop;
