import classes from "./Action.module.css";

const Action = () => {
  return (
    <div className={classes.container}>
      <button className={classes.prescription}>Prescription</button>
      <button className="hover-shake-right">Book an eye test</button>
    </div>
  );
};

export default Action;
