import classes from "./Description-Mobile.module.css";
import Detail from "./Detail";
import Summary from "./Summary";

const DescriptionMobile = () => {
  return (
    <div className={classes.container}>
      <h1 className={classes.title}>
        <span>About the Frames</span>
      </h1>
      <div className={classes["summary-container"]}>
        <Summary />
      </div>
      <Detail />
    </div>
  );
};

export default DescriptionMobile;
