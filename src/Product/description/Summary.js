import classes from "./Summary.module.css";

const Summary = () => {
  return (
    <div className={classes.summary}>
      Rectangular and angular. For looking sharp any day of the week. Parker
      packs a sophisticated yet modern style.
    </div>
  );
};

export default Summary;
