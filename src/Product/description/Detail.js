import { Fragment } from "react";
import classes from "./Detail.module.css";

const Detail = () => {
  return (
    <Fragment>
      <div className={classes.origin}>Made from Acetate</div>
      <div className={classes.size}>
        <div className={classes.fit}>
          <div className={classes.label}>FRAME FIT</div>
          <div>Medium</div>
        </div>
        <div className={classes.measure}>
          <div className={classes.label}>MEASUREMENTS</div>
          <div>52 - 17 - 140</div>
        </div>
      </div>
      <div className={classes.tag}>
        These measurements mean lens width, bridge, and temple length in
        millimetres (mm). They can be found on the inside of the temple arm. If
        you already have a pair that you like, you can compare it to these
        measurements.
      </div>
      <div className={classes.addition}>
        <div>
          Filter category 3 - General purpose sunglasses. High protection
          against sunglare. Good UV Protection.Not suitable for driving in
          twilight, at night or under dull conditions
        </div>
        <div className={classes.note}>
          This frame comes with polarised lenses. An additional $100 will be
          added to your order total for prescription orders.
        </div>
      </div>
    </Fragment>
  );
};

export default Detail;
