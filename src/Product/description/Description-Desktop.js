import classes from "./Description-Desktop.module.css";
import Detail from "./Detail";
import Summary from "./Summary";

const DescriptionDesktop = () => {
  return (
    <div className={classes.container}>
      <h1 className={classes.title}>
        <span>About the Frames</span>
      </h1>
      <div className={classes["description-body"]}>
        <div className={classes["summary-container"]}>
          <Summary />
        </div>
        <div className={classes["detail-container"]}>
          <Detail />
        </div>
      </div>
    </div>
  );
};

export default DescriptionDesktop;
