import classes from "./Color-Icon.module.css";

const ColorIcon = (prop) => {
  const iconClickHandler = () => {
    prop.onColorChange(prop.index);
  };

  return (
    <button
      className={`${classes["color-icon-btn"]} ${
        prop.isSelected ? classes.selected : ""
      }`}
      onClick={iconClickHandler}
    >
      <div
        className={classes["color-icon"]}
        style={{
          backgroundPositionX: prop.positionX + "px",
          backgroundPositionY: prop.positionY + "px",
        }}
      ></div>
    </button>
  );
};

export default ColorIcon;
