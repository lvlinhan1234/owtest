import { Fragment } from "react";
import classes from "./Color-Collection.module.css";
import ColorIcon from "./Color-Icon";

const ColerCollection = (props) => {
  const productColors = props.productColors;
  const colorSelectedIndex = props.colorSelectedIndex;
  const isDesktopOrLaptop = props.isDesktopOrLaptop;

  const colorSelectHandler = (index) => {
    props.onColorSelect(index);
  };

  return (
    <Fragment>
      {!isDesktopOrLaptop && (
        <p className={classes["color-title"]}>
          <span className={classes.label}>Color: &nbsp;</span>
          {productColors[colorSelectedIndex].title}
        </p>
      )}
      <div className={classes["color-collection"]}>
        {productColors.map((color, index) => (
          <ColorIcon
            key={color.id}
            index={index}
            positionX={color.positionX}
            positionY={color.positionY}
            isSelected={index === colorSelectedIndex}
            onColorChange={colorSelectHandler}
          />
        ))}
      </div>
      {isDesktopOrLaptop && (
        <p className={classes["color-title"]}>
          <span className={classes.label}>Color: &nbsp;</span>
          {productColors[colorSelectedIndex].title}
        </p>
      )}
    </Fragment>
  );
};

export default ColerCollection;
