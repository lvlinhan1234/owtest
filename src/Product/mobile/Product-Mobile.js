import { Fragment } from "react";
import ColerCollection from "../color/Color-Collection";
import ProductGallery from "../Product-Gallery";
import Price from "../price/Price";
import classes from "./Product-Mobile.module.css";
import Action from "../action/Action";
import DescriptionMobile from "../description/Description-Mobile";

const ProductMobile = (props) => {
  return (
    <Fragment>
      <div className={classes.heading}>
        <p className={classes.title}>MARCO</p>
        <p className={classes.discount}>2 pairs from $199</p>
        <p className={classes.tag}>Discount auto-applied at checkout.</p>
      </div>
      <ProductGallery productImages={props.productImages} />
      <div className={classes["color-collection-container"]}>
        <ColerCollection
          productColors={props.productColors}
          colorSelectedIndex={props.colorSelectedIndex}
          onColorSelect={props.onColorSelect}
        />
      </div>
      <div className={classes["price-container"]}>
        <Price />
      </div>
      <div className={classes["action-container"]}>
        <Action />
      </div>
      <div className={classes["description-container"]}>
        <DescriptionMobile />
      </div>
    </Fragment>
  );
};

export default ProductMobile;
