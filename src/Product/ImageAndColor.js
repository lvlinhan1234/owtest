const productImages = {
  "Auburn Totoise": [
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_002-auburn-tortoise-front-optical_1.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_002-auburn-tortoise-45d-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_002-auburn-tortoise-side-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_002-auburn-tortoise-back-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_002-auburn-tortoise-flat-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_002_opt-fitpic-2022.jpg",
  ],
  "Bright Cobalt": [
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_401-bright_cobalt-front-optical_1.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_401-bright_cobalt-45d-optical.jpg",
    // "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_401-bright_cobalt-back-optical.jpg",
    // "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_401-bright_cobalt-flat-optical.jpg",
    // "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_401-bright_cobalt-side-optical.jpg",
    // "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_401_opt-fitpic-2022_1.jpg",
  ],
  Champagne: [
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_042-champagne-front-optical_1.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_042-champagne-45d-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_042-champagne-side-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_042-champagne-back-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_042-champagne-flat-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_042_opt-fitpic-2022.jpg",
  ],
  Eclipse: [
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_300s-eclipse-front-optical_1.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_300s-eclipse-45d-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_300s-eclipse-side-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_300s-eclipse-back-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_300s-eclipse-flat-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_300s_opt-fitpic-2022.jpg",
  ],
  Latte: [
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_301s-latte-front-optical_1.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_301s-latte-45d-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_301s-latte-side-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_301s-latte-back-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_301s-latte-flat-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_301s_opt-fitpic-2022.jpg",
  ],
  "Obsidian Black Matte": [
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_016-obsidian-black-matte-front-optical_1.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_016-obsidian-black-matte-45d-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_016-obsidian-black-matte-side-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_016-obsidian-black-matte-back-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_016-obsidian-black-matte-flat-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_016_opt-fitpic-2022.jpg",
  ],
  Spearmint: [
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_350-spearmint-front-optical_1.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_350-spearmint-45d-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_350-spearmint-side-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_350-spearmint-back-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_350-spearmint-flat-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_350_39400_fitpic.jpg",
  ],
  "Striped Buckeye": [
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_031-striped-buckeye-front-optical_1.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_031-striped-buckeye-45d-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_031-striped-buckeye-side-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_031-striped-buckeye-back-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_031-striped-buckeye-flat-optical.jpg",
    "https://dj3im2gm3txew.cloudfront.net/pub/media/catalog/product/cache/57ddb9b9ec2d560f8bdcb4560fc608f2/p/a/parker_031_will_43130_fitpic.jpg",
  ],
};

const productColors = [
  {
    id: "Auburn Totoise",
    title: "Auburn Totoise",
    positionX: -133,
    positionY: -46,
  },
  {
    id: "Bright Cobalt",
    title: "Bright Cobalt",
    positionX: -175,
    positionY: -85,
  },
  {
    id: "Champagne",
    title: "Champagne",
    positionX: -7,
    positionY: -280,
  },
  {
    id: "Eclipse",
    title: "Eclipse",
    positionX: -174,
    positionY: -124,
  },
  {
    id: "Latte",
    title: "Latte",
    positionX: -48,
    positionY: -202,
  },
  {
    id: "Obsidian Black Matte",
    title: "Obsidian Black Matte",
    positionX: -469,
    positionY: -241,
  },
  {
    id: "Spearmint",
    title: "Spearmint",
    positionX: -343,
    positionY: -241,
  },

  {
    id: "Striped Buckeye",
    title: "Striped Buckeye",
    positionX: -217,
    positionY: -7,
  },
];

export { productImages, productColors };
