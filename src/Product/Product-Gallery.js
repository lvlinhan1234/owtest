import { Carousel } from "react-responsive-carousel";
import classes from "./Product-Gallery.module.css";
import { AiOutlineZoomIn } from "react-icons/ai";
import { useEffect, useRef, useState } from "react";
import ProductZoom from "./Product-Zoom";

const ProductGallery = (props) => {
  const productImages = props.productImages;
  const corouselRef = useRef();
  const [showZoom, setShowZoom] = useState(false);
  const [currentIndex, setCurrentIndex] = useState(0);

  useEffect(() => {
    corouselRef.current.moveTo(0);
  }, [props]);

  return (
    <div className={classes.gallery}>
      <Carousel
        ref={corouselRef}
        showArrows={true}
        autoPlay={false}
        infiniteLoop={true}
        emulateTouch={true}
        showThumbs={false}
        dynamicHeight={false}
        showStatus={false}
        onChange={(index) => {
          setCurrentIndex(index);
        }}
        // onChange={onChange}
        // onClickItem={onClickItem}
        // onClickThumb={onClickThumb}
      >
        {productImages.map((productImage) => (
          <div key={productImage}>
            <img src={productImage} alt="glass-img" />
            {/* <p className="legend">Legend 1</p> */}
          </div>
        ))}
      </Carousel>
      <button className={classes.showZoomBtn} onClick={() => setShowZoom(true)}>
        <span>Zoom</span>
        <AiOutlineZoomIn size={20} />
      </button>
      {showZoom && (
        <ProductZoom
          productImages={productImages}
          setShowZoom={setShowZoom}
          index={currentIndex}
        />
      )}
    </div>
  );
};

export default ProductGallery;
