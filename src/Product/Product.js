import { Fragment, useState } from "react";
import ProductMobile from "./mobile/Product-Mobile";
import { useMediaQuery } from "react-responsive";
import ProductDesktop from "./desktop/Product-Desktop";
import { productImages, productColors } from "./ImageAndColor";

const Product = () => {
  const [colorSelectedIndex, setColorSelectedIndex] = useState(0);
  const isDesktopOrLaptop = useMediaQuery({
    query: "(min-width: 1224px)",
  });
  const changeColor = (index) => {
    setColorSelectedIndex(index);
  };
  const colorSelectedId = productColors[colorSelectedIndex].id;
  const productSelectedImages = productImages[colorSelectedId];

  return (
    <Fragment>
      {!isDesktopOrLaptop && (
        <ProductMobile
          productImages={productSelectedImages}
          productColors={productColors}
          colorSelectedIndex={colorSelectedIndex}
          onColorSelect={changeColor}
        />
      )}
      {isDesktopOrLaptop && (
        <ProductDesktop
          productImages={productSelectedImages}
          productColors={productColors}
          colorSelectedIndex={colorSelectedIndex}
          onColorSelect={changeColor}
        />
      )}
    </Fragment>
  );
};

export default Product;
