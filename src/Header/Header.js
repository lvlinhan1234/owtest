import { Fragment } from "react";
import HeaderMobile from "./mobile/Header-Mobile";
import { useMediaQuery } from "react-responsive";
import HeaderDesktop from "./desktop/Header-Desktop";

const Header = () => {
  const isDesktopOrLaptop = useMediaQuery({
    query: "(min-width: 1224px)",
  });
  return (
    <Fragment>
      {!isDesktopOrLaptop && <HeaderMobile />}
      {isDesktopOrLaptop && <HeaderDesktop />}
    </Fragment>
  );
};

export default Header;
