import classes from "./Header-Mobile.module.css";
import { useState } from "react";
import { BsList } from "react-icons/bs";
import { MdOutlineShoppingBag } from "react-icons/md";
import Badge from "@mui/material/Badge";
import SlideNav from "./Slide-Nav";

const HeaderMobile = () => {
  const [showSlideNav, setShowSlideNav] = useState(false);
  const showSlideNavHandler = () => {
    setShowSlideNav(true);
  };
  const hideSlideNavHandler = () => {
    setShowSlideNav(false);
  };
  return (
    <header className={classes.header}>
      <div className={classes.highlight}>
        <button className={classes.eyetest_btn}>BOOK AN EYE TEST</button>
      </div>
      <div className={classes["header-body"]}>
        <div className="icon-container" onClick={showSlideNavHandler}>
          <BsList size={30} />
        </div>
        <img className={classes.logo} src="./assets/logo.jpg" alt="logo"></img>
        <div className="icon-container">
          <Badge
            badgeContent={2}
            color="secondary"
            anchorOrigin={{
              vertical: "bottom",
              horizontal: "right",
            }}
          >
            <MdOutlineShoppingBag size={30} />
          </Badge>
        </div>
      </div>
      <SlideNav
        showSlideNav={showSlideNav}
        onHideSlideNav={hideSlideNavHandler}
      />
    </header>
  );
};

export default HeaderMobile;
