import classes from "./Nav-Item.module.css";

const NavItem = (prop) => {
  return <li className={classes["nav-item"]}>{prop.title}</li>;
};

export default NavItem;
