import { Fragment } from "react";
import NavItem from "./Nav-Item";
import classes from "./Slide-Nav.module.css";
import { FiArrowLeft } from "react-icons/fi";

const SlideNav = (props) => {
  const navTitles = [
    "Glasses",
    "Sunglasdes",
    "Locations",
    "Eye Test Info",
    "Offers",
    "My Account",
    "Shopping Cart",
  ];
  const navList = navTitles.map((item) => <NavItem key={item} title={item} />);
  const showSlideNav = props.showSlideNav;
  const slideNavClass = `${classes["slide-nav"]} ${
    showSlideNav ? classes["slide-show"] : ""
  }`;
  return (
    <Fragment>
      {showSlideNav && (
        <div className={classes.overlay} onClick={props.onHideSlideNav}></div>
      )}
      <div className={slideNavClass}>
        <div
          className="icon-container margin-left-0"
          onClick={props.onHideSlideNav}
        >
          <FiArrowLeft size={30} />
        </div>
        <ul className={classes.navList}>{navList}</ul>
      </div>
    </Fragment>
  );
};

export default SlideNav;
