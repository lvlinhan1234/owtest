import classes from "./Header-Desktop.module.css";
import { BsSearch, BsPerson } from "react-icons/bs";
import { MdOutlineShoppingBag } from "react-icons/md";
import Badge from "@mui/material/Badge";

const HeaderDesktop = () => {
  return (
    <div className={classes.container}>
      <div className={classes.header}>
        <a href="/" className={classes.logo}>
          <img src="./assets/logo.jpg" alt="logo"></img>
        </a>
        <div className={classes["navbar-container"]}>
          <div className={classes.navbar}>
            <div>
              <a href="/">Glasses</a>
            </div>
            <div>
              <a href="/">Sunglasdes</a>
            </div>
            <div>
              <a href="/">Locations</a>
            </div>
            <div>
              <a href="/">Eye Test Info</a>
            </div>
            <div>
              <a href="/">Offers</a>
            </div>
          </div>
        </div>
        <button className={`${classes.book} hover-shake-right`}>
          BOOK AN EYE TEST
        </button>
        <div className={classes["icon-group"]}>
          <div className={classes["icon-container"]}>
            <BsSearch size={20} />
          </div>
          <div className={classes["icon-container"]}>
            <BsPerson size={25} />
          </div>
          <div className={classes["icon-container"]}>
            <Badge
              badgeContent={2}
              color="secondary"
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "right",
              }}
            >
              <MdOutlineShoppingBag size={25} />
            </Badge>
          </div>
        </div>
      </div>
    </div>
  );
};

export default HeaderDesktop;
