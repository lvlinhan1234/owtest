import "./App.css";
import Header from "./Header/Header";
import Product from "./Product/Product";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import { useSelector } from "react-redux";

function App() {
  const hideScrollV = useSelector((state) => state.global.hideScrollV);
  return (
    <div className="App" style={{ overflowY: hideScrollV ? "hidden" : "auto" }}>
      <Header />
      <Product />
    </div>
  );
}

export default App;
